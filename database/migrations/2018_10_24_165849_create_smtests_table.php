<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmtestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smtests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->bigInteger('quantity');
            $table->integer('buy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smtests');
    }
    public  function SetTitleAttribute($value)
    {
        $this->attributes['title']=$value;
        $this->attribute['slug']=str_slug($value);
    }
    public function getRouteKeyTitle()
    {
        return 'slug';
    }
}
