<?php

namespace App\Http\Controllers;

use App\Http\Requests\SmtestForm;
use App\Smtest;
use Illuminate\Http\Request;

class SmtestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $smtest=Smtest::all();
        return view('smtest.index',compact('smtest'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('smtest.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SmtestForm  $request)
    {
        Smtest::create($request->all());
        return redirect('smtest');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Smtest  $smtest
     * @return \Illuminate\Http\Response
     */
    public function show(Smtest $smtest)
    {
        return view('smtest.show',compact('smtest'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Smtest  $smtest
     * @return \Illuminate\Http\Response
     */
    public function edit(Smtest $smtest)
    {
        return view('smtest.edit',compact('smtest'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Smtest  $smtest
     * @return \Illuminate\Http\Response
     */
    public function update( Smtest $smtest , SmtestForm $request)
    {
        $smtest->update($request->all());
        return redirect('smtest');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Smtest  $smtest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Smtest $smtest)
    {
        $smtest->delete();
        return redirect('smtest/create');
    }
}
