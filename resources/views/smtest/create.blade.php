@extends('layout.master')
@section('content')
<form method="post" action="{{route('smtest.create')}}">
    @include('smtest.form',
    ['smtest'=>new App\smtest,])
</form>
@endsection