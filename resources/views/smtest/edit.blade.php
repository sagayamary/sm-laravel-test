@extends('layout.master')
@section('content')
    <form method="post" action="{{route('smtest.update',['testcase'=>$testcase->id])}}">
        @include('smtest.form',
        ['smtest'=>new App\smtest,])
    </form>
@endsection