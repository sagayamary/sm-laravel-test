@csrf
<div class="container">
<div class="form-group">
        <label for="formGroupExampleInput">title</label>
        <input type="text" name="title" class="form-control" id="formGroupExampleInput" placeholder="Example input" value="{{old('title')??$smtest->title}}">
    </div>
    <div class="form-group">
        <label for="sel1">Quantity :</label>
        <input type="number" name="quantity" class="form-control" id="formGroupExampleInput" placeholder="Example input" value="{{old('quantity')??$smtest->quantity}}">

    </div>
    <div class="form-group">
        <label for="formGroupExampleInput">buy</label>
        <input type="text" name="buy" class="form-control" id="formGroupExampleInput" placeholder="Example input" value="{{old('buy')??$smtest->buy}}">
    <button>
        {{$SubmitButtonText??'Create New task'}}
    </button>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
