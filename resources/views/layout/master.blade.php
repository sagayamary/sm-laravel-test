<!DOCTYPE html>
<html lang="en">
@include('layout.head')
<body id="page-top">
@include('layout.header')
@include('layout.nav')
@yield('content')
@include('layout.footer')
@include('layout.script')
</body>
</html>
